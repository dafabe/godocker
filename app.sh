docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker-compose down
docker-compose build
docker-compose up -d
docker ps

# docker build . -t datapro
# docker run --publish 8080:8080 --detach --name app datapro
# docker ps
# docker exec -it gd /bin/bash