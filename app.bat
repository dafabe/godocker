docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker build . -t datapro
docker run --publish 3000:3000 --detach --name app datapro
docker ps