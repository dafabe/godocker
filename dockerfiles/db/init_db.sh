#!/bin/sh

mysql -uroot -p$MYSQL_ROOT_PASSWORD  -e "grant all privileges on *.* to '$MYSQL_USER'@'%' identified by '$MYSQL_PASSWORD';"
mysql -uroot -p$MYSQL_ROOT_PASSWORD  -e "flush privileges;"
