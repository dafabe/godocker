

SHOW DATABASES;


create database task_man;

CREATE TABLE  task_man.tasks (
    ID int NOT NULL AUTO_INCREMENT,
    Description varchar(255) NOT NULL,
    PRIMARY KEY (ID)
);

ALTER TABLE task_man.tasks ADD COLUMN status varchar(20) AFTER description;

describe task_man.tasks;


/* add 3 tasks */
INSERT INTO task_man.tasks (Description, status )
VALUES ('take out bin', 'open' );

INSERT INTO task_man.tasks (Description, status )
VALUES ('check email', 'open' );

INSERT INTO task_man.tasks (Description, status )
VALUES ('take vit d', 'open' );

select * from task_man.tasks;