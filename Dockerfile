FROM golang:1.12

ENV GO111MODULE=on
ENV CGO_ENABLED=0

WORKDIR /app/server
COPY go.mod .
COPY go.sum .

RUN go mod download
COPY . .

RUN go build -o main .
EXPOSE 8080:8080
CMD ["./main"]
